Proyecto del curso “Python 3: Curso completo” (UDEMY)
Proyecto centrado en los fundamentos de Python.

El objetivo de este ejercicio fue realizar un código para manejo de creación y herencia de objetos.
Fue el primer ejercicio realizado con Python.

Se crean objetos de tipo figura geométrica, con diferentes colores, altos y anchos.
Y un archivo de tipo test para realizar las pruebas por consola.
